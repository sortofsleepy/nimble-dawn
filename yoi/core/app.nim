import staticglfw

type
  Sketch* = ref object of RootObj
    window:Window

# Setup for the program
proc setup*(self:Sketch, width:int32, height:int32, title:string) =
  #init glfw
  if init() == 0:
    raise newException(Exception, "Failed to Initialize GLFW")

  # window hint GLFW_CLIENT_API, GLFW_NO_API - Dawn provides
  #windowHint(0x00022001,0)
  self.window = createWindow(width,height,title,nil,nil)

# returns window of Sketch
proc get_window*(self:Sketch):Window = return self.window

# Starts the draw loop
proc draw*(self:Sketch,render:proc()) =
  while windowShouldCLose(self.window) == 0:

    self.window.swapBuffers()
    pollEvents()

  self.window.destroyWindow()
  terminate()


#[

type
  GLFWwindow{.importcpp: "GLFWwindow *", pure, header:"webgpu/webgpu_glfw.h"} =  object

proc SetupWindow(win:GLFWwindow){.importcpp:"Print(#)".}
discard init()

# window hint GLFW_CLIENT_API, GLFW_NO_API - Dawn provides
windowHint(0x00022001,0)
var window = createWindow(1024,768,"Test",nil,nil)
var obj = cast[GLFWwindow](window)
SetupWindow(obj)

]#