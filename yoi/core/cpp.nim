
###### UNIQUE_PTR ######
type
  CppUniquePtr*[T] {.importcpp: "std::unique_ptr", header: "<memory>", byref.}  = object

# Creates a unique pointer around an object type
proc make_unique*[T]():CppUniquePtr[T] {.importcpp: "std::make_unique<'*0>()", header: "<memory>", constructor.}

# calls reset on a unique pointer
proc reset*[T](self:CppUniquePtr[T]) {.importcpp: "reset".}

# Gets underlying reference to object. Should be eqivalant to calling object->method once methods are implemented.
proc get*[T](self:CppUniquePtr[T]):ptr T {.importcpp: "get".}


##### VECTORS #####
type
  CppVector*[T] {.importcpp"std::vector", header: "<vector>", byref.} = object

# https://github.com/mratsim/agent-smith/blob/a2d9251e289f92f6b5fb68e19a98d16b00f2694c/third_party/std_cpp.nim
proc size*(v: CppVector): int {.importcpp: "#.size()", header: "<vector>".}
proc begin*[T](l: CppVector[T]): CppVector[T] {.importcpp: "begin", header: "<vector>".}
proc `[]`*[T](v: CppVector[T], idx: int): T{.importcpp: "#[#]", header: "<vector>".}
proc `[]`*[T](v: var CppVector[T], idx: int): var T{.importcpp: "#[#]", header: "<vector>".}
proc `[]`*[T](it: CppVector[T]): T       {.importcpp: "*#", header: "<vector>".}
proc next*[T](it: CppVector[T]; n = 1): CppVector[T] {.importcpp: "next(@)", header: "<iterator>".}

proc CppVectorToSeq*[T](l: CppVector[T]): seq[T] =
  result = newSeq[T](l.size())
  for i in 0..<l.size():
    result[i] = l[i]

  return result
