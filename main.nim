{.passC:"-I" & "libraries/dawn/include"}
{.passC:"-I" & "dawn"}
{.link:"libraries/dawn/dawn.lib".}
{.link: "libraries/glfw/glfw3.lib" .}

import staticglfw
import dawn/lib
import yoi/core/cpp
import yoi/core/app
import dawn/proc_table

#proc init_device(window:Window, instance:CppUniquePtr[Instance]){.importcpp:"CreateVulkanDevice(@)", header:"helpers.h".}

var prog = app.Sketch()
prog.setup(1024,768,"Test")

# setup WebGPU
var wgpu = lib.create_instance()
var wgpu_ref = wgpu.get()
wgpu_ref.DiscoverDefaultAdapters()


wgpu_ref.DiscoverDefaultAdapters()
var adapters = wgpu_ref.GetAdapters()

var adapter_seq = cpp.CppVectorToSeq(adapters)
var adapter = lib.FindVulkan(adapter_seq)
var device = adapter.CreateDevice()
var procs = lib.GetProcs()
var win = prog.get_window()
var desc = lib.GetSurfaceDescriptor(lib.GLFWCast(win))
var surfaceDesc = lib.WGPUSurfaceDescriptor()
surfaceDesc.nextInChain = lib.ChainedStructToWGPUChainedStruct(desc.get())

procs.instanceCreateSurface(wgpu_ref.GetWGPUInstance(),addr surfaceDesc)


#[

var win = prog.get_window()
var desc = lib.GetSurfaceDescriptor(lib.GLFWCast(win))
var surfaceDesc = lib.WGPUSurfaceDescriptor()
surfaceDesc.nextInChain = lib.ChainedStructToWGPUChainedStruct(desc.get())

]#