{.passC:"-I" & "ref"}
# https://github.com/mratsim/agent-smith/search?q=CppUniquePtr
# testing how we might wrap a class.
# part one - non-ptr
# https://forum.nim-lang.org/t/8429
#[
type
  Instance* {.importcpp:"Test", header:"library.h".} = object


proc echo(self:Instance) {.importcpp: "echo" .}

var a = Instance()
a.echo()
]#


# Part 2 - testing wrapping smart ptr + calling
#[
type
  CppUniquePtr[T] {.importcpp: "std::unique_ptr", header: "<memory>", byref.}  = object
type
  Instance* {.importcpp: "Test", header:"library.h" .} = object
  Dawn* = CppUniquePtr[Instance]
  DawnRef = ptr Instance


#proc make_unique*[T]():CppUniquePtr[T] {.importcpp: "std::make_unique<'*0>()", header: "<memory>", constructor.}
proc make_unique*[T]():Dawn {.importcpp: "std::make_unique<'*0>()", header: "<memory>", constructor.}

proc create_ref(self:CppUniquePtr[Instance]):DawnRef {.importcpp: "get".}
proc print(self:ptr Instance) {.importcpp: "print".}

var a = make_unique[Instance]()
var p = a.create_ref()
p.print()


]#

type
  CppUniquePtr[T] {.importcpp: "std::unique_ptr", header: "<memory>", byref.}  = object
type
  Instance* {.importcpp: "Test", header:"library.h" .} = object
    backend_type:cint
  Dawn* = CppUniquePtr[Instance]
  DawnRef = ptr Instance


#proc make_unique*[T]():CppUniquePtr[T] {.importcpp: "std::make_unique<'*0>()", header: "<memory>", constructor.}
proc make_unique*[T]():Dawn {.importcpp: "std::make_unique<'*0>()", header: "<memory>", constructor.}
proc reset*[T](self:CppUniquePtr[T]) {.importcpp: "reset".}
proc get*[T](self:CppUniquePtr[T]):ptr T {.importcpp: "get".}

proc print(self:ptr Instance) {.importcpp: "print".}

var a = make_unique[Instance]()
var p = a.get()
if p.backend_type == 1:
  echo "YES"


