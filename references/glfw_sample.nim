# assumes you've installed nim-glfw
# build with -d:glfwStaticLib


import os, glfw

proc main =
  glfw.initialize()

  var c = DefaultOpenglWindowConfig
  c.title = "Test"

  var w = newWindow(c)

  while true:
    glfw.swapBuffers(w)
    glfw.pollEvents()

    if glfw.shouldClose(w):
      break

  w.destroy()
  glfw.terminate()
main()
