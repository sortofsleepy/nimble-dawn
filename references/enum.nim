

{.emit: """

    #include <stdint.h>
    enum class BackendType : uint32_t {
        Null = 0x00000000,
        WebGPU = 0x00000001,
        D3D11 = 0x00000002,
        D3D12 = 0x00000003,
        Metal = 0x00000004,
        Vulkan = 0x00000005,
        OpenGL = 0x00000006,
        OpenGLES = 0x00000007,
    };


    struct AdapterProperties {
        BackendType backendType;
    };


""".}

type
  BackendType {.importcpp:"BackendType".} = ptr object
    Vulkan*:cuint

type
  AdapterProperties* {.importcpp.} = object
    backendType*:BackendType


var a:cuint = 0x00000005
var b:cuint = 0x00000005

if a == b :
  echo "YES"