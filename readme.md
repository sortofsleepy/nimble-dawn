# NimbleDawn 

An experiment with WebGPU and [Nim](https://nim-lang.org/) 


Building 
== 
This was tested with Visual Studio on Windows. The following assumes you will also be using windows. 

* Download and install [Dawn](https://dawn.googlesource.com/dawn) according to the directions. There are, however, some potential pitfalls as described below. 
* You will also need to download [GLFW](https://www.glfw.org/download.html). You can choose to build yourself or download one of the pre-built binaries. 
* This will use [staticglfw](https://github.com/treeform/staticglfw) as the wrapper for GLFW

Gotchas
===
* Nim, by default, focuses on using MinGW in Windows which can cause problems in regard to DirectX as well as some Windows apis that MinGW does not bundle. Issues will arise with 
  * `d3d11on12.h`
  * `dxcapi.h`
  * `DXProgrammableCapture.h`
  * `windows.ui.core.h>`
  * `<windows.ui.xaml.controls.h>`
  
Recent versions of MinGW will include `d3d11on12.h` and you can obtain `dxcapi.h` by installing the [DirectXShader Compiler](https://github.com/microsoft/DirectXShaderCompiler).
* From what I understand, there might be issues with DirectXShaderCompiler and MinGW;  @slimsag has been working on a similar library for Zig and has graciously put up [a fork of the compiler](https://github.com/hexops/DirectXShaderCompiler) and made it more compatible with MinGW. 


That said

* Fortunately Nim supports using compilers other than MinGW, including VCC. With that being the case, you should be able to build Dawn with Visual Studio. 
  * Note that in the past, there were issues with DirectX still, I tested with Vulkan.
* A Debug build may not work for an unknown reason, but a Release build will. A release build should be fine; even if an error is encountered, the symbols appear to be mostly readable.
* Before you build, in `Surface.cpp`, you can comment line 107. It uses a Windows api that does not appear to exist on a normal install of Windows and the Windows sdk. Dawn appears to be preparing to support
native windowing but for some reason the necessary api just simply doesn't exist. Currently, it does not seem to cause any issues. 
* You'll also have to make some changes to your `nim.cfg` file (under the `config` folder of your Nim install)
  * change the `cc` option so it says `cc=vcc`
  * for the option `vcc.options.always` and `vcc.cpp.options.always` add the `/MD` flag.
* GLFW normally can be used just as a static library but `nim-glfw` appears to make use of a generated `.dll` file that GLFW can be built with; you will need to make sure to build accordingly and 
keep the `.dll` the same root path as your main Nim file.


# Necessary libraries
Currently, it appears that you will need to make sure you can link to the following libraries(will continue to update as needed)
```nim
{. link:"libraries/dawn/common/dawn_common.lib" .}
{. link:"libraries/dawn/common/dawn_glfw.lib" .}

{. link:"libraries/dawn/core/dawn_native.lib" .}
{. link:"libraries/dawn/core/dawn_platform.lib" .}
{. link:"libraries/dawn/core/SPIRV-Tools.lib" .}
{. link:"libraries/dawn/core/SPIRV-Tools-opt.lib" .}
{. link:"libraries/dawn/core/webgpu_dawn.lib" .}
{. link:"libraries/dawn/core/dawn_utils.lib" .}
{. link:"libraries/dawn/core/dawn_wire.lib" .}
{. link:"libraries/dawn/core/dawn_proc.lib" .}
{. link:"libraries/dawn/core/dawncpp.lib" .}
{. link:"libraries/dawn/core/dawncpp_headers.lib" .}
{. link:"libraries/dawn/core/dawn_headers.lib" .}

{. link:"libraries/dawn/core/dawn_headers.lib" .}
{. link:"libraries/dawn/core/dawn_proc.lib" .}
{. link:"libraries/dawn/core/dawncpp.lib" .}
{. link:"libraries/dawn/core/dawncpp_headers.lib" .}

{. link:"libraries/dawn/tint/tint.lib" .}
{. link:"libraries/dawn/tint/tint_diagnostic_utils.lib" .}
{. link:"libraries/dawn/tint/tint_utils_io.lib" .}
{. link:"libraries/dawn/tint/tint_val.lib" .}
{. link:"libraries/dawn/common/dawn_common.lib" .}

{.link: "libraries/dawn/absl/absl_str_format_internal.lib" .}
{.link: "libraries/dawn/absl/absl_strings_internal.lib" .}
{.link: "libraries/dawn/absl/absl_strings.lib" .}
{.link: "libraries/dawn/absl/absl_int128.lib" .}
{.link: "libraries/dawn/absl/absl_base.lib" .}
{.link: "libraries/dawn/absl/absl_raw_logging_internal.lib" .}
{.link: "libraries/dawn/absl/absl_throw_delegate.lib" .}
```

And everything should build!

# Merging libraries
According to [this](https://stackoverflow.com/questions/13492365/how-to-merge-two-windows-vc-static-library-into-one), it appears that you can merge all of these libraries into one. 
You will need to run the command from the Visual Studio console.