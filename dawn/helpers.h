//
// Created by sortofsleepy on 1/24/2023.
//

#ifndef NIMBLE_DAWN_HELPERS_H
#define NIMBLE_DAWN_HELPERS_H

#include "dawn/dawn_proc.h"
#include "dawn/dawn_wsi.h"
#include "dawn/native/DawnNative.h"
#include "dawn/wire/WireClient.h"
#include "dawn/wire/WireServer.h"
#include "webgpu/webgpu_glfw.h"

// Helper function to build a vulkan device.
wgpu::Device CreateVulkanDevice(
        GLFWwindow* window,
        std::unique_ptr<dawn::native::Instance> & instance
        ) {

    if (!window) {
        return wgpu::Device();
    }


    // Get an adapter for the backend to use, and create the device.
    dawn::native::Adapter backendAdapter;
    std::vector<dawn::native::Adapter> adapters = instance->GetAdapters();
    auto adapterIt = std::find_if(adapters.begin(), adapters.end(),
                                  [](const dawn::native::Adapter adapter) -> bool {
                                      wgpu::AdapterProperties properties;
                                      adapter.GetProperties(&properties);
                                      return properties.backendType == wgpu::BackendType::Vulkan;
                                  });
    backendAdapter = *adapterIt;

    WGPUDevice backendDevice = backendAdapter.CreateDevice();
    DawnProcTable backendProcs = dawn::native::GetProcs();

    // Create the swapchain
    auto surfaceChainedDesc = wgpu::glfw::SetupWindowAndGetSurfaceDescriptor(window);
    WGPUSurfaceDescriptor surfaceDesc;
    surfaceDesc.nextInChain = reinterpret_cast<WGPUChainedStruct*>(surfaceChainedDesc.get());
    WGPUSurface surface = backendProcs.instanceCreateSurface(instance->Get(), &surfaceDesc);

    WGPUSwapChainDescriptor swapChainDesc;
    swapChainDesc.usage = WGPUTextureUsage_RenderAttachment;
    swapChainDesc.format = static_cast<WGPUTextureFormat>(wgpu::TextureFormat::BGRA8Unorm);
    swapChainDesc.width = 640;
    swapChainDesc.height = 480;
    swapChainDesc.presentMode = WGPUPresentMode_Mailbox;
    swapChainDesc.implementation = 0;
    WGPUSwapChain backendSwapChain =
            backendProcs.deviceCreateSwapChain(backendDevice, surface, &swapChainDesc);

    // Choose whether to use the backend procs and devices/swapchains directly, or set up the wire.
    WGPUDevice cDevice = nullptr;
    DawnProcTable procs;

    procs = backendProcs;
    cDevice = backendDevice;
    //swapChain = wgpu::SwapChain::Acquire(backendSwapChain);

    dawnProcSetProcs(&procs);
    //procs.deviceSetUncapturedErrorCallback(cDevice, PrintDeviceError, nullptr);

    return wgpu::Device::Acquire(cDevice);
}
#endif //NIMBLE_DAWN_HELPERS_H
