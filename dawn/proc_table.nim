import ../yoi/core/cpp

# Defines the proc functions to run.

type
  DawnProcTable*{.importcpp:"DawnProcTable", header:"dawn/dawn_proc_table.h".} = object


proc instanceCreateSurface*[T,A](self:DawnProcTable, instance:T, desc:ptr A){.importcpp:"#.instanceCreateSurface(@)",  header:"dawn/dawn_proc_table.h".}

