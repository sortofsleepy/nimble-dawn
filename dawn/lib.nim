import ../yoi/core/cpp
import constants
import proc_table
import staticglfw

######################### TYPES #####################################

# This is needed for C++ functions that pass a normal GLFWwindow* object around
# TODO maybe reference glfw itself instead of Dawn library.
type
  GLFWwindow{.importcpp: "GLFWwindow *", pure, header:"webgpu/webgpu_glfw.h"} = object


type
  Instance* {.importcpp: "dawn::native::Instance", header:"dawn/native/DawnNative.h" .} = object
  InstanceRef* = ptr Instance
  WGPUInstance*{.importcpp:"WGPUInstance", header:"webgpu/webgpu.h".} = pointer

proc GetWGPUInstance*(self:InstanceRef):WGPUInstance{.importcpp:"#->Get()".}
type
  AdapterProps* {.importcpp:"wgpu::AdapterProperties", header:"webgpu/webgpu_cpp.h".} = object
    # defined in wgpu_cpp.h
    backendType*:uint
  Adapter* {.importcpp: "dawn::native::Adapter", header:"dawn/native/DawnNative.h" .} = object

type
  WGPUDevice*{.importcpp:"WGPUDevice", header:"webgpu/webgpu_cpp.h".} = object

type
  ChainedStruct*{.importcpp:"wgpu::ChainedStruct", header:"webgpu/webgpu_cpp.h".} = object
  ChainedStructPtr* = CppUniquePtr[ChainedStruct]
  WGPUChainedStruct*{.importcpp:"WGPUChainedStruct*", header:"webgpu/webgpu_cpp.h".} = object

type
  WGPUSurfaceDescriptor*{.importcpp:"WGPUSurfaceDescriptor", header:"webgpu/webgpu_cpp.h".} = object
    nextInChain*:WGPUChainedStruct

type
  WGPUTextureFormat{.importcpp:"WGPUTextureFormat",header:"webgpu/webgpu_cpp.h".} = object

# Note that Swapchain descriptor needs to be initialized with values; default initialization will throw an error
#[
  You can't do something like this
  WGPUSwapChainDescriptor swapChainDesc;
  swapChainDesc.width = 640;
  swapChainDesc.height = 480;


  but have to do something like

  var a = SwapChainDescriptor(width:400, height:400, <etc...>)
  ... etc'

  CPP that gets generated looks something like

  var a = SwapChainDescriptor{0,0,0,0}
]#
type
  WGPUSwapchainDescriptor*{.importcpp:"WGPUSwapChainDescriptor", header:"webgpu/webgpu_cpp.h".} = object
    nextInChain*:WGPUChainedStruct
    usage*:uint32
    format*:WGPUTextureFormat
    width*:uint32
    height*:uint32
    presentMode*:uint32
    implementation*:uint64


######################### METHODS #####################################

# casts window pointer to GLFWwindow pointer which is needed with the GLFW wrapper library
proc GLFWCast*(win:Window):Window{.importcpp:"static_cast<GLFWwindow*>(#)".}

# creates the WebGPU device
proc CreateDevice*(self:Adapter):WGPUDevice{.importcpp:"#.CreateDevice()".}

# return adapter properties
proc GetAdapterProps(self:Adapter, prop_obj:ptr AdapterProps){.importcpp:"#.GetProperties(@)".}

# Discovers default adapters on an Instance object.
proc DiscoverDefaultAdapters*(self:InstanceRef) {.importcpp: "DiscoverDefaultAdapters".}

# return available adapters
proc GetAdapters*(self:InstanceRef):CppVector[Adapter] {.importcpp: "GetAdapters".}

# return back end functions necessary for some things
proc GetProcs*():proc_table.DawnProcTable{.importcpp:"dawn::native::GetProcs()".}

# get surface descriptor for window
proc GetSurfaceDescriptor*(window:Window):ChainedStructPtr {.importcpp:"wgpu::glfw::SetupWindowAndGetSurfaceDescriptor(#)", header:"webgpu/webgpu_glfw.h".}

proc ChainedStructToWGPUChainedStruct*[T](chain:ptr T):WGPUChainedStruct{.importcpp:"reinterpret_cast<WGPUChainedStruct*>(#)",header:"webgpu/webgpu_cpp.h".}

# Looks for Vulkan backend type in list of possible adapters.
proc FindVulkan*(adapters:seq[Adapter]):Adapter =
  for i in 0..<adapters.len:
    var prop = AdapterProps()
    adapters[i].GetAdapterProps(addr prop)

    # TODO not totally sure what to do here - backendType is populated correctly but can't compare without cast
    if cast[uint](prop.backendType) == ord(constants.BackEnd.VK):
      return adapters[i]



# Creates instance - returns unique_ptr to a Dawn Instance object.
proc create_instance*():CppUniquePtr[Instance] =
  return cpp.make_unique[Instance]()


